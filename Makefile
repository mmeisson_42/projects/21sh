# *************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/08/28 14:28:00 by mmeisson          #+#    #+#              #
#    Updated: 2018/12/19 12:22:00 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			= 21sh

CC				= gcc

CFLAGS			= -MD -Wall -Werror -Wextra -Ofast

VPATH			= ./src

SRCS			 = shell.c ft_environment.c ft_get_command.c ft_init_datas.c\
		   ft_print_prompt.c ft_rputchar.c ft_error.c ft_shutdown.c ft_action.c\
		   ft_copypast.c ft_cursor.c ft_del.c ft_homend.c ft_enter.c\
		   ft_history.c ft_mvword.c ft_lexerparser.c ft_lexer.c ft_parser.c\
		   ft_pars_identifier1.c ft_pars_identifier2.c ft_get_lexem.c\
		   ft_liblex.c ft_hash.c ft_redirections.c ft_sherialize.c ft_echo.c\
		   ft_env.c ft_unsetenv.c ft_setenv.c ft_exit.c \
		   ft_cd.c ft_export.c ft_unset.c ft_pathjoin.c ft_interpret.c\
		   ft_errno_library.c ft_signal.c ft_init_command.c ft_berrors.c\
		   ft_sher_access.c ft_heredoc.c ft_strlib.c ft_redlib.c ft_select.c\
		   ft_getter_parser.c ft_cdpaths.c ft_add_char.c ft_parsedel.c\
		   ft_built_envlib.c ft_hashbis.c ft_sher_built.c ft_get_hist.c

INCS_PATHS		= ./inc/ ./src/ ./libft/incs/
INCS			= $(addprefix -I,$(INCS_PATHS))

OBJS_PATH		= ./.objs/
OBJS_NAME		= $(SRCS:.c=.o)
OBJS			= $(addprefix $(OBJS_PATH), $(OBJS_NAME))


DEPS			= $(OBJS:.o=.d)

LIB_PATHS		= ./libft/
LIBS			= $(addprefix -L,$(LIB_PATHS))

LDFLAGS			= $(LIBS) -lft -ltermcap



all: $(NAME)


$(NAME): $(OBJS)
	@$(foreach PATHS, $(LIB_PATHS),\
		echo "# # # # # #\n#";\
		echo '# \033[31m' Compiling $(PATHS) '\033[0m';\
		echo "#\n# # # # # #";\
		make -j32 -C $(PATHS);\
	)
	$(CC) $^ -o $@ $(LDFLAGS)

$(OBJS_PATH)%.o: %.c Makefile
	@mkdir -p $(OBJS_PATH)
	$(CC) $(CFLAGS) $(INCS) -o $@ -c $<

self_clean:
	rm -rf $(OBJS_PATH)

clean: self_clean
	@$(foreach PATHS, $(LIB_PATHS), make -C $(PATHS) clean;)

self_fclean:
	rm -rf $(OBJS_PATH)
	rm -f $(NAME)

fclean: self_fclean
	@$(foreach PATHS, $(LIB_PATHS), make -C $(PATHS) fclean;)

re:
	make fclean
	make -j32 all

-include $(DEPS)

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rputchar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 07:33:27 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 06:12:37 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int			ft_rputchar(int c)
{
	return (write(1, (char*)&c, 1));
}

void		ft_stringtab_del(char ***str)
{
	char	**tmp;
	size_t	i;

	i = 0;
	tmp = *str;
	while (tmp[i])
		free(tmp[i++]);
	free(tmp);
	*str = NULL;
}

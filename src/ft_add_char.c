/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_add_char.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 03:43:02 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/04 03:43:23 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void				ft_add_char(char add, t_command *c)
{
	if (c->len + 1 >= c->buff_size)
	{
		c->buffer = ft_strrealloc(c->buffer, c->buff_size + BUFFER_SIZE);
		c->buff_size += BUFFER_SIZE;
	}
	if (c->select->len != c->select->pos)
	{
		tputs(tgetstr("cd", NULL), 1, ft_rputchar);
		ft_putchar(add);
		tputs(tgetstr("sc", NULL), 1, ft_rputchar);
		ft_putstr(c->buffer + c->pos);
		tputs(tgetstr("rc", NULL), 1, ft_rputchar);
	}
	else
		ft_putchar(add);
	ft_memmove(c->buffer + c->pos + 1, c->buffer + c->pos, c->len - c->pos);
	(c->buffer)[c->pos] = add;
	c->select->pos++;
	c->pos++;
	c->select->len++;
	c->len++;
}

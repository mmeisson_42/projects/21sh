/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/31 19:42:09 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 19:05:30 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int		ft_get_sherialize(char **av, char **env)
{
	size_t			i;
	t_start_node	sher;

	i = 0;
	ft_bzero(&sher, sizeof(t_start_node));
	while (av[i])
		i++;
	sher.exec = *av;
	sher.argc = i;
	sher.argv = av;
	return (ft_sherialize(&sher, env));
}

char	**ft_get_env(char ***args)
{
	char			**av;
	static char		*env[256];
	size_t			i;

	av = *args;
	i = 0;
	if (*av)
	{
		while (av[i] && ft_strchr(av[i], '=') && i < 256)
		{
			env[i + 1] = av[i];
			i++;
		}
		*args = av + i;
	}
	env[i + 1] = NULL;
	return (env);
}

int		ft_delenv_option(int ac, char ***argv, t_env *benv)
{
	int		ret;
	char	**av;

	av = *argv;
	ret = 0;
	while (ac > 0 && *av != NULL && **av == '-')
	{
		if (!ft_strequ("-u", *av))
		{
			ret = ft_opt_error("env [-i] [-u name] [var=content]",
					av[0][1]);
			break ;
		}
		else if (!av[1])
		{
			ret = ft_opt_error("env [-i] [-u name] [var=content]",
					av[0][1]);
			break ;
		}
		else
			ft_envdel(av[1], benv);
		av += 2;
	}
	*argv = av;
	return (0);
}

void	ft_subenv(char **av, t_env *benv)
{
	ft_export(0, ft_get_env(&av), benv);
	if (!*av)
		ft_print_env(benv);
	else
		ft_get_sherialize(av, benv->env);
}

int		ft_env(int ac, char **av, t_env *env)
{
	t_env			benv;
	int				ret;
	int				i;
	pid_t			f;

	ret = 0;
	i = 0;
	f = fork();
	if (f == 0)
	{
		ft_bzero(&benv, sizeof(t_env));
		av++;
		if (ft_empty_env(&av) == 0)
			ft_fill_env(&benv, env->env);
		ret = ft_delenv_option(ac, &av, &benv);
		if (ret == 0)
			ft_subenv(av, &benv);
		exit(ret);
	}
	wait(&ret);
	return (WEXITSTATUS(ret));
}

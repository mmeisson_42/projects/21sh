/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cdpaths.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 02:54:34 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/04 02:56:10 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void			ft_trimslash(char *str)
{
	char		*tmp;
	size_t		i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '/')
		{
			tmp = str + ++i;
			while (*tmp == '/')
				tmp++;
			ft_strcpy(str + i, tmp);
		}
		i++;
	}
	i = ft_strlen(str) - 1;
	if (i > 0 && str[i] == '.')
		str[i--] = 0;
	if (i > 0 && str[i] == '/')
		str[i] = 0;
}

void			ft_resolve_dot(char *str)
{
	int		i;
	int		tmp;

	i = 1;
	while (str[i])
	{
		if (ft_strequ(str + i, "/.") || ft_strnequ(str + i, "./", 2))
			ft_strcpy(str + i, str + i + 2);
		else if (ft_strnequ(str + i, "../", 3) ||
				ft_strnequ(str + i, "/..", 3) || ft_strequ(str + i, ".."))
		{
			tmp = i + 3;
			if (str[i] == '/')
				i--;
			while (str[i] != '/' && i > 1)
				i--;
			ft_strcpy(str + i, str + tmp);
			if (i > 0)
				i--;
		}
		else
			i++;
	}
	ft_trimslash(str);
}

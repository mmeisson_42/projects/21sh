/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_shutdown.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 08:18:33 by mmeisson          #+#    #+#             */
/*   Updated: 2016/04/23 15:45:14 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

extern t_global		g_datas;

void		ft_shutdown(int error_code)
{
	tcsetattr(1, TCSADRAIN, &(g_datas.origin));
	exit(error_code);
}

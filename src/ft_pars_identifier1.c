/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pars_identifier1.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/29 13:00:48 by mmeisson          #+#    #+#             */
/*   Updated: 2016/04/23 15:35:28 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int		ft_parse_exec(char *str, t_type last)
{
	static int		(*fct[])() = {
		ft_parse_pipe,
		ft_parse_redirection,
		ft_parse_stop,
		NULL,
	};
	int				i;

	i = 0;
	while (fct[i])
	{
		if (fct[i](str, last) == 0)
			return (1);
		i++;
	}
	return (0);
}

int		ft_parse_option(char *str, t_type last)
{
	static int		(*fct[])() = {
		ft_parse_pipe,
		ft_parse_redirection,
		ft_parse_stop,
		NULL,
	};
	int				i;

	i = 0;
	while (fct[i])
	{
		if (fct[i](str, last) == 0)
			return (1);
		i++;
	}
	return (0);
}

int		ft_parse_pipe(char *str, t_type last)
{
	if (last)
		;
	if (ft_strequ(str, "|"))
		return (0);
	return (1);
}

int		ft_parse_redirection(char *str, t_type last)
{
	if (last)
		;
	if (ft_strequ(str, ">") || ft_strequ(str, ">>") ||
			ft_strequ(str, "<") || ft_strequ(str, "<<"))
		return (0);
	return (1);
}

int		ft_parse_file_desc(char *str, t_type last)
{
	if (last == REDIRECTION)
	{
		if (*str != '&')
			return (1);
		else
			str++;
	}
	if (ft_strlen(str) == 1 && ((*str >= '0' && *str <= '2') || *str == '-'))
		return (0);
	return (1);
}

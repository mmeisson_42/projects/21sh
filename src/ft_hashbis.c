/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hashbis.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 03:52:51 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/04 03:54:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

size_t			get_hash(const char *key)
{
	size_t			ret;
	unsigned char	*ukey;

	ret = 0;
	ukey = (unsigned char*)key;
	while (*ukey)
	{
		ret = ret * MULTIPLIER + *ukey;
		ukey++;
	}
	return (ret % TABLE_SIZE);
}

char			*ft_get_hash_value(const char *key, t_hash *table[TABLE_SIZE])
{
	size_t		index;
	t_hash		*h;

	index = get_hash(key);
	h = table[index];
	while (h)
	{
		if (ft_strequ(key, h->key))
			return (h->value);
		h = h->next;
	}
	return (NULL);
}

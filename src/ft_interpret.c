/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_interpret.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 00:54:34 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 12:02:26 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

extern t_env		*g_env;

char		*ft_get_next_word(char **word)
{
	char		*str;
	char		*ret;
	size_t		i;

	i = 0;
	str = *word;
	while (str[i] && str[i] != ' ' && str[i] != '\t' && str[i] != '"' &&
			str[i] != '\'')
		i++;
	if ((ret = ft_strndup(str, i)) == NULL)
		ft_malloc_error();
	*word = str + i;
	return (ret);
}

char		*ft_get_specs(char **ret, char **tmp, char **str)
{
	while (**tmp && **tmp != '$')
	{
		if (**tmp == '~')
		{
			if (!(*ret = ft_strover(*ret,
							ft_get_variables(ft_strdup("$HOME")))))
				ft_malloc_error();
			*str = *tmp + 1;
		}
		(*tmp)++;
	}
	if (!*tmp || **tmp == '\0')
	{
		if ((*ret = ft_strover(*ret, *str)) == NULL)
			ft_malloc_error();
		return (*ret);
	}
	(*tmp)++;
	return (NULL);
}

char		*ft_get_variables(char *str)
{
	char		*ret;
	char		*var;
	char		*tmp;

	ret = NULL;
	tmp = str;
	while (str)
	{
		if (ft_get_specs(&ret, &tmp, &str))
			return (ret);
		if (tmp != str)
			if ((ret = ft_strnover(ret, str, (tmp - str) - 1)) == NULL)
				ft_malloc_error();
		if ((var = ft_get_next_word(&tmp)))
			if ((ret = ft_strover(ret, ft_getenv(var, g_env->env))) == NULL)
				ft_malloc_error();
		ft_strdel(&var);
		str = tmp;
	}
	return (NULL);
}

char		*ft_interpret(char *str)
{
	char		*ret;
	char		c;
	size_t		len;

	ret = NULL;
	if (*str != '\'')
		ret = ft_get_variables(str);
	else if ((ret = ft_strdup(str)) == NULL)
		ft_malloc_error();
	if (*ret == '\'' || *ret == '"')
	{
		c = *ret;
		ft_strcpy(ret, ret + 1);
		len = ft_strlen(ret);
		if (ret[len - 1] == c)
			ret[len - 1] = '\0';
	}
	ft_strdel(&str);
	return (ret);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 08:06:49 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/11 15:41:10 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void		ft_malloc_error(void)
{
	g_errno = ENOMEM;
	ft_perror("");
	ft_shutdown(g_errno);
}

void		ft_term_error(t_tererr e)
{
	const char		*error[] = {
		"21sh : Fatal Error : TERM variable not set\n",
		"21sh : Fatal Error : TERM entry doesn't exist or database not found\n",
		"21sh : Fatal Error : Termios structure not found\n",
	};

	ft_putstr(error[e]);
	exit(1);
}

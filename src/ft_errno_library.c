/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errno_library.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 20:32:46 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 03:51:47 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

static const char			*ft_errnolib1(t_errno e)
{
	const char		*err[] = {
		"No error",
		"Operation not permitted",
		"No such file or directory",
		"No such process",
		"Interrupted system call",
		"Input/output error",
		"Device not configured",
		"Argument list too long",
		"Bad file descriptor",
		"No child processes",
		"Resource deadlock avoided",
		"Resource temporarily unavailable",
		"Cannot allocate memory",
		"Permission denied",
		"Bad address",
	};

	if (e < 15)
		return (err[e]);
	return (NULL);
}

static const char			*ft_errnolib2(t_errno e)
{
	const char		*err[] = {
		"Block device required",
		"Device / Resource busy",
		"File exists",
		"Cross-device link",
		"Operation not supported by device",
		"Not a directory",
		"Is a directory",
		"Invalid argument",
		"Too many open files in system",
		"Too many open files",
		"Inappropriate ioctl for device",
		"Text file busy",
		"File too large",
		"No space left on device",
		"Illegal seek",
	};

	if (e >= 15 && e < 30)
		return (err[e - 15]);
	return (NULL);
}

static const char			*ft_errnolib3(t_errno e)
{
	const char		*err[] = {
		"Read-only file system",
		"Too many links",
		"Broken pipe",
		"Numerical argument out of domain",
		"Result too large",
		"Resource temporarily unavailable",
		"Operation now in progress",
		"Operation already in progress",
		"Command not found",
	};

	if (e >= 30 && e < 39)
		return (err[e - 30]);
	return (NULL);
}

static const char			*ft_default(t_errno e)
{
	const char		*def_message = "Unknown error";

	if (e > 37)
		return (def_message);
	return (def_message);
}

int							ft_perror(const char *str)
{
	const char		*name = "21sh: ";
	const char		*(*fct)(t_errno);
	char			*ret;

	if (g_errno < 15)
		fct = ft_errnolib1;
	else if (g_errno >= 15 && g_errno < 30)
		fct = ft_errnolib2;
	else if (g_errno >= 30 && g_errno < 39)
		fct = ft_errnolib3;
	else
		fct = ft_default;
	if (!(ret = ft_strjoin(name, fct(g_errno))))
		ft_shutdown(ENOMEM);
	if (*str)
	{
		ft_putstr_fd(ret, 2);
		ft_putstr_fd(" : ", 2);
		ft_putendl_fd((char *)str, 2);
	}
	else
		ft_putendl_fd(ret, 2);
	free(ret);
	return (g_errno);
}

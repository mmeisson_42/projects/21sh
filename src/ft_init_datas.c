/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_datas.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 07:35:08 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 05:22:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

extern t_global		g_datas;
extern t_hash		*g_table[TABLE_SIZE];

void			ft_init_term(void)
{
	char			*name;
	struct termios	t;

	if (!(name = getenv("TERM")))
		ft_term_error(TERM_NFOUND);
	if (tgetent(NULL, name) < 1)
		ft_term_error(TERM_NFOUND);
	else if (tcgetattr(0, &t) == -1)
		ft_term_error(TERMIOS_NFOUND);
	ft_memcpy(&(g_datas.origin), &t, sizeof(struct termios));
	t.c_lflag &= ~(ICANON | ECHO);
	t.c_cc[VMIN] = 1;
	t.c_cc[VTIME] = 0;
	tcsetattr(0, TCSADRAIN, &t);
	tputs(tgetstr("am", NULL), 1, ft_rputchar);
	tputs(tgetstr("bw", NULL), 1, ft_rputchar);
}

t_env			*ft_init_datas(char **env)
{
	t_env		*e;

	e = ft_envinit(env);
	ft_load_hash_paths(ft_getenv("PATH", env), g_table);
	g_datas.proc_status = 1;
	ft_init_term();
	return (e);
}

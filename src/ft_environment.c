/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_environment.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 06:40:30 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 19:42:30 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

t_env		*ft_envinit(char **env)
{
	t_env		*e;
	char		*lvl;

	if (!(e = ft_memalloc(sizeof(t_env))))
		ft_malloc_error();
	while (env[e->env_size])
	{
		if (!((e->env)[e->env_size] = ft_strdup(env[e->env_size])))
			ft_malloc_error();
		e->env_size++;
	}
	if (!(lvl = ft_getenv("SHLVL", e->env)))
		ft_envadd("SHLVL", "1", e);
	else
	{
		if (!(lvl = ft_itoa(ft_atoi(lvl) + 1)))
			ft_malloc_error();
		ft_envadd("SHLVL", lvl, e);
		free(lvl);
	}
	return (e);
}

char		*ft_getenv(char *src, char **environ)
{
	size_t		len;
	size_t		i;

	i = 0;
	len = ft_strlen(src);
	if (len > 0)
		while (environ[i])
		{
			if (ft_strnequ(src, environ[i], len) &&
					(!environ[i][len] || environ[i][len] == '='))
				return (environ[i] + len + 1);
			i++;
		}
	return (NULL);
}

int			ft_envadd(char *var, char *content, t_env *e)
{
	char		*env;
	size_t		len;
	ssize_t		i;

	if (!e)
		return (EXIT_FAILURE);
	if (!(env = ft_strjoin(var, "=")) ||
			!(env = ft_strover(env, content)))
		ft_malloc_error();
	len = ft_strlen(var);
	i = -1;
	while ((e->env)[++i])
	{
		if (ft_strnequ((e->env)[i], var, len))
		{
			free((e->env)[i]);
			(e->env)[i] = env;
			return (EXIT_SUCCESS);
		}
	}
	if (e && e->env_size >= ENV_MAX_SIZE - 2)
		return (EXIT_FAILURE);
	(e->env)[i] = env;
	e->env_size++;
	return (EXIT_SUCCESS);
}

int			ft_envdel(char *var, t_env *e)
{
	size_t		len;
	size_t		i;

	i = 0;
	len = ft_strlen(var);
	while ((e->env)[i])
	{
		if (ft_strnequ(var, (e->env)[i], len))
		{
			free((e->env[i]));
			while ((e->env[i]))
			{
				(e->env[i]) = (e->env)[i + 1];
				i++;
			}
			e->env_size--;
			return (EXIT_SUCCESS);
		}
		i++;
	}
	return (EXIT_FAILURE);
}

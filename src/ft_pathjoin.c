/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pathjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 23:52:35 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 05:31:39 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

char	*ft_pathjoin(char *p1, char *p2)
{
	char		*ret;

	ret = NULL;
	if (p1[ft_strlen(p1) - 1] != '/' && ft_strlen(p1) > 1)
	{
		if (!(ret = ft_strjoin(p1, "/")) ||
				!(ret = ft_strover(ret, p2)))
			ft_malloc_error();
	}
	else if (!(ret = ft_strjoin(p1, p2)))
		ft_malloc_error();
	return (ret);
}

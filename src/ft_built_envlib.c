/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_built_envlib.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 03:46:14 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 18:56:20 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int		ft_empty_env(char ***av)
{
	size_t		i;
	int			ret;
	char		**argv;

	i = 1;
	ret = 0;
	argv = *av;
	while (*argv && ft_strequ(*argv, "-i"))
	{
		ret = 1;
		argv++;
	}
	*av = argv;
	return (ret);
}

void	ft_fill_env(t_env *benv, char **env)
{
	char	**e;

	e = benv->env;
	while (*env)
	{
		if (!(*(e++) = ft_strdup(*(env++))))
			ft_malloc_error();
		benv->env_size++;
	}
	*e = NULL;
}

void	ft_print_env(t_env *e)
{
	char		**environ;

	environ = e->env;
	if (environ)
	{
		while (*environ)
		{
			ft_putendl(*environ);
			environ++;
		}
	}
}

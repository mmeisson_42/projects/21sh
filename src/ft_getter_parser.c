/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getter_parser.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 02:28:29 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 20:19:27 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void					ft_manage_exec(t_start_node *node, t_lex *l,
		t_strstr *args)
{
	t_str	*argv;
	char	**tab;
	size_t	len;
	size_t	i;

	len = (args->str) ? args->str->length + 2 : 2;
	i = 0;
	if (!(tab = ft_memalloc(sizeof(char*) * len)))
		ft_malloc_error();
	if (!(tab[i++] = ft_strdup(l->str)))
		ft_malloc_error();
	argv = args->str;
	while (argv)
	{
		if (!(tab[i++] = ft_strdup(argv->str)))
			ft_malloc_error();
		argv = argv->next;
	}
	node->argc = (int)i;
	node->exec = tab[0];
	node->argv = tab;
}

static t_start_node		*ft_get_node(t_type type, char *str)
{
	t_start_node	*new;

	if (!(new = ft_memalloc(sizeof(t_start_node))))
		ft_malloc_error();
	if (type == STOP)
	{
		if (ft_strequ("&&", str))
			new->cond = 1;
		else if (ft_strequ("||", str))
			new->cond = -1;
	}
	return (new);
}

void					ft_get_red(t_lex **l, t_start_node *node)
{
	t_redir		*red;

	if (!(red = ft_memalloc(sizeof(t_redir))))
		ft_malloc_error();
	if ((*l)->type == FILE_DESC)
	{
		red->first_part = ft_strdup((*l)->str);
		*l = (*l)->next;
	}
	red->type = ft_strdup((*l)->str);
	if ((*l)->next && ((*l)->next->type == FILE_DESC ||
				(*l)->next->type == FILE_NAME))
	{
		*l = (*l)->next;
		red->sec_part = ft_strdup((*l)->str);
	}
	ft_redadd(&(node->redir), red);
}

void					ft_startaddpipe(t_start_node **head, t_start_node *node)
{
	t_start_node	*tmp;

	if (!*head)
		node = *head;
	else
	{
		tmp = *head;
		while (tmp->pipe)
			tmp = tmp->pipe;
		tmp->pipe = node;
	}
}

t_start_node			*ft_get_parse(t_lex *l, t_strstr *args)
{
	t_start_node	*parse;
	t_start_node	*head;

	if (!(parse = ft_memalloc(sizeof(t_start_node))))
		ft_malloc_error();
	head = parse;
	while (l)
	{
		if (l->type == STOP && l->next)
		{
			parse = ft_get_node(STOP, l->str);
			parse->next = head;
			head = parse;
		}
		else if (l->type == PIPE)
		{
			parse = ft_get_node(PIPE, l->str);
			ft_startaddpipe(&head, parse);
		}
		else
			ft_subgetter(&l, &parse, &args);
		l = l->next;
	}
	return (head);
}

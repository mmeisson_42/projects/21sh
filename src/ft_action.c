/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_action.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 14:55:12 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 20:45:38 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int			ft_historic_command(char *buffer, t_command *command,
		t_command **historic)
{
	const t_fct		fct[] = {
		{ ARR_UP, ft_arrup },
		{ ARR_DOWN, ft_arrdown },
	};
	int				i;

	i = -1;
	while (++i < 2)
		if (ft_strequ(buffer, fct[i].str))
		{
			fct[i].fct(command, historic);
			return (EXIT_SUCCESS);
		}
	return (EXIT_FAILURE);
}

int			ft_action(char *buffer, t_command *command, t_command **historic)
{
	const t_fct		fct[] = {
		{ DEL, ft_deleteback },
		{ DELETE, ft_delete },
		{ HOME, ft_home },
		{ END, ft_end },
		{ OPT_UP, ft_optup },
		{ OPT_DOWN, ft_optdown },
		{ OPT_LEFT, ft_optleft },
		{ OPT_RIGHT, ft_optright },
		{ ARR_LEFT, ft_arrleft },
		{ ARR_RIGHT, ft_arrright },
		{ O_V, ft_optv },
		{ SELECT, ft_select },
	};
	int				i;

	i = -1;
	while (++i < 12)
		if (ft_strequ(buffer, fct[i].str))
		{
			fct[i].fct(command);
			return (EXIT_SUCCESS);
		}
	return (ft_historic_command(buffer, command, historic));
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mvword.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 17:10:47 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/01 05:01:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void		ft_optright(t_command *command)
{
	t_line		*l;
	char		*buffer;
	t_command	*c;

	c = command;
	l = c->select;
	buffer = c->buffer;
	while (buffer[c->pos] != ' ' && l->pos < l->len)
		ft_arrright(command);
	while (buffer[c->pos] == ' ' && l->pos < l->len)
		ft_arrright(command);
}

void		ft_optleft(t_command *command)
{
	t_line		*l;
	char		*buffer;
	t_command	*c;

	c = command;
	l = c->select;
	buffer = c->buffer;
	if (buffer[c->pos] != ' ' && l->pos > 0)
		ft_arrleft(command);
	while (buffer[c->pos] == ' ' && l->pos > 0)
		ft_arrleft(command);
	while (buffer[c->pos] != ' ' && l->pos > 0)
		ft_arrleft(command);
	if (buffer[c->pos] == ' ' && l->pos < l->len)
		ft_arrright(command);
}

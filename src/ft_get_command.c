/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_command.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 06:27:18 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 22:06:22 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

extern t_global		g_datas;

size_t				ft_parse_command(char *s)
{
	char	*str;

	str = s;
	while (*str)
	{
		if (*str == '\\')
		{
			if (str[1] == '\n')
				ft_strcpy(str, str + 2);
			else
				ft_strcpy(str, str + 1);
		}
		else if (*str == '\'' || *str == '"')
			str = ft_strchr(str + 1, *str);
		str++;
	}
	return (str - s);
}

static void			ft_free_command(t_command **node)
{
	t_line		*tmp;

	if (*node)
	{
		free((*node)->buffer);
		tmp = NULL;
		while ((*node)->line)
		{
			tmp = (*node)->line;
			(*node)->line = (*node)->line->next;
			free(tmp);
		}
		free(*node);
		*node = NULL;
	}
}

static void			ft_del_histcopy(t_command *hist)
{
	t_command	*tmp;
	t_command	*h;

	h = hist;
	while (h->prev)
		h = h->prev;
	tmp = NULL;
	while (h)
	{
		tmp = h;
		h = h->next;
		if (tmp == hist)
		{
			tmp->next = NULL;
			tmp->prev = NULL;
		}
		else
			ft_free_command(&tmp);
	}
}

static t_command	*ft_validate(t_command *command)
{
	if (ft_enter(command) == EXIT_SUCCESS)
	{
		command->len = ft_parse_command(command->buffer);
		ft_del_histcopy(command);
		return (command);
	}
	return (NULL);
}

t_command			*ft_get_command(t_command *history)
{
	t_command				*command;
	t_command				*hist_copy;
	int						ret;
	char					debug[13];

	command = ft_init_command();
	hist_copy = ft_get_hist(command, history);
	while (42)
	{
		ft_strclr(debug);
		if ((ret = read(0, debug, 12)) &&
			ft_action(debug, command, &hist_copy) == EXIT_FAILURE && ret == 1)
		{
			if (*debug == 4 && !*(command->buffer))
				ft_shutdown(0);
			if (*debug != 4 && *debug != '\t' && *debug != '\n' && ret == 1)
				ft_add_char(*debug, command);
			else if (*debug == '\n' && ret == 1)
				if (ft_validate(command))
					return (command);
		}
	}
	return (command);
}

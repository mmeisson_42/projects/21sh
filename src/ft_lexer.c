/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lexer.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 14:15:49 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 20:04:48 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

static char			*ft_get_lexem(char **command)
{
	char			*lexem;
	size_t			i;
	static char		*(*fct[])(char**) = {
		ft_is_stop,
		ft_is_pipe,
		ft_is_red,
		ft_is_text,
	};

	i = 0;
	while (i < 4)
	{
		if ((lexem = fct[i](command)))
		{
			if (i == 3)
				lexem = ft_interpret(lexem);
			return (lexem);
		}
		i++;
	}
	return (NULL);
}

t_lex				*ft_lexer(char *str)
{
	char			*command;
	t_lex			*lexem;
	t_lex			*tmp;
	char			*t;

	lexem = NULL;
	if (str)
	{
		if (!(str = ft_strtrim(str)))
			ft_malloc_error();
		if (!(command = ft_strjoin(str, ";")))
			ft_malloc_error();
		t = command;
		tmp = NULL;
		while (*command)
		{
			while (*command == ' ')
				command++;
			ft_lexadd(&lexem, ft_lexnew(ft_get_lexem(&command)));
		}
		ft_strdel(&t);
		ft_strdel(&str);
	}
	return (lexem);
}

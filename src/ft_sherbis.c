/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sherbis.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 02:56:34 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/04 03:02:40 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

extern t_hash		*g_table[TABLE_SIZE];
extern t_global		g_datas;
extern t_env		*g_env;

static void		ft_save_manager(int get)
{
	static int	save[2] = { -1, -1 };

	if (get == 0)
	{
		save[0] = dup(0);
		save[1] = dup(1);
	}
	else
	{
		dup2(save[0], 0);
		dup2(save[1], 1);
	}
}

static int		ft_built_pipe(t_start_node *start, int (*fct)(),
		char **environ)
{
	int		desc[2];
	int		ret;

	pipe(desc);
	ft_save_manager(1);
	close(desc[1]);
	dup2(desc[0], 0);
	ft_manage_descriptors(start->redir);
	ret = fct(start->argc, start->argv, environ);
	ft_sherialize(start->pipe, desc, environ);
	ft_save_manager(0);
	return (ret);
}

char			*ft_get_from_path(char *exec, char **environ)
{
	char		*path;
	char		**paths;
	char		*cmd;
	size_t		i;

	g_errno = OK;
	if (!(path = ft_getenv("PATH", environ)) || !*path)
		path = "/usr/bin/:/bin/";
	if (!(paths = ft_strsplit(path, ':')))
		ft_malloc_error();
	i = -1;
	while (paths[++i])
	{
		cmd = ft_pathjoin(paths[i], exec);
		if (ft_sher_access(cmd) == OK)
		{
			ft_stringtab_del(&paths);
			return (cmd);
		}
	}
	ft_stringtab_del(&paths);
	return (NULL);
}

int				(*ft_get_builtins(char *exec))()
{
	const t_int_fct		builtins[8] = {
		{ "cd", ft_cd },
		{ "env", ft_env },
		{ "export", ft_export },
		{ "unset", ft_unset },
		{ "setenv", ft_setenv },
		{ "unsetenv", ft_unsetenv },
		{ "echo", ft_echo },
		{ "exit", ft_exit },
	};
	int					i;

	i = -1;
	while (++i < 8)
	{
		if (ft_strequ(exec, builtins[i].str))
			return (builtins[i].fct);
	}
	return (NULL);
}

int				ft_exec_builtin(t_start_node *start, int (*fct)(),
		char **environ)
{
	int		ret;

	if (environ == NULL)
		environ = g_env->env;
	if (start->pipe)
		ret = ft_built_pipe(start, fct, environ);
	else
	{
		ft_save_manager(1);
		ret = fct(start->argc, start->argv, environ);
		ft_manage_descriptors(start->redir);
		ft_save_manager(0);
	}
	if (g_errno != 0)
		ft_perror("");
	return (ret);
}

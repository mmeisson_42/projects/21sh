/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_liblex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 16:48:32 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 20:15:56 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

t_lex		*ft_lexnew(char *str)
{
	t_lex		*node;

	if (!(node = malloc(sizeof(t_lex))))
		ft_malloc_error();
	node->str = str;
	node->next = NULL;
	return (node);
}

void		ft_lexadd(t_lex **head, t_lex *node)
{
	t_lex		*tmp;

	if (!*head)
		*head = node;
	else
	{
		tmp = *head;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = node;
	}
}

void		ft_subgetter(t_lex **l, t_start_node **parse,
		t_strstr **args)
{
	if ((*l)->type == EXEC)
	{
		ft_manage_exec(*parse, *l, *args);
		*args = (*args)->next;
	}
	else if ((*l)->type == FILE_DESC || (*l)->type == REDIRECTION)
		ft_get_red(l, *parse);
}

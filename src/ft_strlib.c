/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlib.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 05:32:03 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 05:35:17 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void		ft_stradd(t_str **lst, char *str)
{
	t_str		*tmp;

	if (!*lst)
	{
		if (!(*lst = ft_memalloc(sizeof(t_str))))
			ft_malloc_error();
		(*lst)->str = str;
		(*lst)->length = 1;
	}
	else
	{
		tmp = *lst;
		while (tmp->next)
			tmp = tmp->next;
		if (!(tmp->next = ft_memalloc(sizeof(t_str))))
			ft_malloc_error();
		tmp->next->str = str;
		(*lst)->length += 1;
	}
}

void		ft_strstrdel(t_strstr *s)
{
	t_str	*tmp;
	t_str	*tmp2;

	if (s)
	{
		tmp = s->str;
		while (tmp)
		{
			tmp2 = tmp;
			tmp = tmp->next;
			free(tmp2);
		}
		free(s);
	}
}

void		ft_strdellast(t_str **s)
{
	t_str	*tmp;
	t_str	*last;

	tmp = *s;
	if (!(tmp->next))
	{
		free(tmp);
		*s = NULL;
	}
	else
	{
		while (tmp->next)
		{
			last = tmp;
			tmp = tmp->next;
		}
		free(tmp);
		last->next = NULL;
	}
}

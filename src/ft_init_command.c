/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_command.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/28 01:38:43 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 14:44:47 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void			ft_commandadd(t_command **head, t_command *node)
{
	t_command		*tmp;

	if (!*head)
		*head = node;
	else
	{
		tmp = *head;
		while (tmp->next)
			tmp = tmp->next;
		node->prev = tmp;
		tmp->next = node;
	}
}

void			ft_commandadd_front(t_command **head, t_command *node)
{
	if (!*head)
		*head = node;
	else
	{
		node->next = *head;
		node->prev = NULL;
		(*head)->prev = node;
		*head = node;
	}
}

void			ft_lineadd(t_line **head, t_line *dup)
{
	t_line		*tmp;
	t_line		*node;

	if (!(node = ft_memdup(dup, sizeof(t_line))))
		ft_malloc_error();
	if (!*head)
		*head = node;
	else
	{
		tmp = *head;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = node;
	}
}

t_command		*ft_historicdup(t_command *history, t_command *command)
{
	t_command	*ret;
	t_command	*node;

	ret = NULL;
	while (history)
	{
		if (!(node = ft_memdup(history, sizeof(t_command))))
			ft_malloc_error();
		if (!(node->line = ft_memalloc(sizeof(t_line))))
			ft_malloc_error();
		if (!(node->buffer = ft_strdup(history->buffer)))
			ft_malloc_error();
		node->buff_size = ft_strlen(node->buffer);
		node->line->len = node->len;
		node->next = NULL;
		node->prev = NULL;
		ft_commandadd(&ret, node);
		history = history->next;
	}
	ft_commandadd(&ret, command);
	return (ret);
}

t_command		*ft_init_command(void)
{
	t_command	*node;

	if (!(node = ft_memalloc(sizeof(t_command))))
		ft_malloc_error();
	if (!(node->line = ft_memalloc(sizeof(t_line))))
		ft_malloc_error();
	if (!(node->buffer = ft_strnew(BUFFER_SIZE)))
		ft_malloc_error();
	node->buff_size = BUFFER_SIZE;
	node->select = node->line;
	node->line->first_x = 0;
	return (node);
}

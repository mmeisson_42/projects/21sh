/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_del.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 15:51:52 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 20:40:58 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void		ft_delete(t_command *command)
{
	t_line		*l;
	char		*buffer;

	l = command->select;
	buffer = command->buffer;
	if (l->pos < l->len)
	{
		ft_strcpy(buffer + l->pos, buffer + l->pos + 1);
		l->len--;
		command->len--;
		tputs(tgetstr("cd", NULL), 1, ft_rputchar);
		tputs(tgetstr("sc", NULL), 1, ft_rputchar);
		ft_putstr(buffer + command->pos);
		tputs(tgetstr("rc", NULL), 1, ft_rputchar);
	}
}

void		ft_deleteback(t_command *command)
{
	t_line		*l;
	char		*buffer;

	l = command->select;
	buffer = command->buffer;
	if (l->pos > 0)
	{
		ft_strcpy(buffer + command->pos - 1, buffer + command->pos);
		l->len--;
		command->len--;
		ft_arrleft(command);
		tputs(tgetstr("sc", NULL), 1, ft_rputchar);
		tputs(tgetstr("cd", NULL), 1, ft_rputchar);
		ft_putstr(buffer + command->pos);
		tputs(tgetstr("rc", NULL), 1, ft_rputchar);
	}
}

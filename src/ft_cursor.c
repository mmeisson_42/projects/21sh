/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cursor.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 15:49:58 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 06:21:37 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void		ft_optdown(t_command *command)
{
	struct winsize	w;
	size_t			i;

	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	i = 0;
	while (i++ < w.ws_col)
		ft_arrright(command);
}

void		ft_optup(t_command *command)
{
	struct winsize	w;
	size_t			i;

	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	i = 0;
	while (i++ < w.ws_col)
		ft_arrleft(command);
}

void		ft_arrleft(t_command *command)
{
	t_line			*l;
	size_t			ret;
	struct winsize	w;

	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	l = command->select;
	if (l->pos > 0)
	{
		l->pos--;
		command->pos--;
		if ((command->buffer)[command->pos] == '\n')
		{
			ret = (w.ws_col % command->pos) + command->line->first_x + 1;
			tputs(tgetstr("up", NULL), 1, ft_rputchar);
			while (--ret)
				tputs(tgetstr("nd", NULL), 1, ft_rputchar);
			l->pos--;
			command->pos--;
		}
		else
			tputs(tgetstr("le", NULL), 1, ft_rputchar);
	}
}

void		ft_arrright(t_command *command)
{
	t_line			*l;
	struct winsize	w;

	l = command->select;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	if (l->pos < l->len)
	{
		if ((l->pos + l->first_x) % w.ws_col == w.ws_col - 1)
			tputs(tgetstr("do", NULL), 1, ft_rputchar);
		else
			tputs(tgetstr("nd", NULL), 1, ft_rputchar);
		l->pos++;
		command->pos++;
		if ((command->buffer)[command->pos] == '\n')
		{
			l->pos++;
			command->pos++;
			tputs(tgetstr("do", NULL), 1, ft_rputchar);
		}
	}
}

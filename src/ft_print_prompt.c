/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_prompt.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 06:27:53 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 06:29:18 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

extern t_global		g_datas;
extern t_env		*g_env;

size_t			ft_print_prompt(void)
{
	const char	prompt[] = " %> ";
	char		*tx;
	char		*tmp;
	char		*search;
	size_t		ret;

	if (!(tx = ft_strnew(PROMPT_SIZE)))
		ft_malloc_error();
	if (!(tmp = ft_getenv("PWD", g_env->env)))
		tmp = getcwd(NULL, PROMPT_SIZE);
	if ((search = ft_strrchr(tmp, '/')))
		ft_strncpy(tx, search + 1, PROMPT_SIZE - 5);
	else
		ft_strncpy(tx, search, PROMPT_SIZE - 5);
	ft_strcat(tx, prompt);
	ret = ft_strlen(tx);
	ft_putstr(L_GREEN);
	ft_putstr(tx);
	ft_putstr(DEFAULT);
	free(tx);
	return (ret);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_heredoc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 02:53:27 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 21:46:14 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

extern char		*g_heredoc;

static void		ft_get_heredoc(char *word)
{
	char		*buf;
	ssize_t		i;

	buf = NULL;
	while (1)
	{
		i = 0;
		ft_strdel(&buf);
		ft_putstr("heredoc> ");
		while (!buf || buf[i++] != '\n')
		{
			if (i % 255 == 0)
				if (!(buf = ft_strrealloc(buf, sizeof(char*) * (i + 255))))
					ft_malloc_error();
			read(0, buf + i, 1);
			ft_putchar(buf[i]);
		}
		if (ft_strnequ(word, buf, i - 1))
		{
			ft_strdel(&buf);
			return ;
		}
		if (!(g_heredoc = ft_strover(g_heredoc, buf)))
			ft_malloc_error();
	}
}

static char		*ft_get_word(char *str)
{
	char		*ret;
	size_t		i;

	i = 0;
	while (*str == ' ')
		str++;
	while (str[i] && str[i] != ' ' && !ft_strchr(QUOTES, str[i])
			&& !ft_strchr(OPEN, str[i]))
		i++;
	if (!*str)
		return (NULL);
	if (!(ret = ft_strndup(str, i)))
		ft_malloc_error();
	return (ret);
}

void			ft_heredoc(char *str)
{
	char		*word;

	ft_putchar('\n');
	while (*str)
	{
		if (ft_strnequ(str, "<<", 2))
		{
			str += 2;
			if ((word = ft_get_word(str)))
				ft_get_heredoc(word);
			ft_strdel(&word);
			return ;
		}
		str++;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_enter.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 16:04:20 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 19:13:08 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

char		*g_heredoc = NULL;

ssize_t			ft_istrchr(char *str, char c)
{
	ssize_t		ret;

	ret = 0;
	while (str[ret])
	{
		if (str[ret] == c)
			return (ret);
		ret++;
	}
	return (-1);
}

static char		ft_pipeslash(char *str, char *buffer)
{
	while (str < buffer && *(--buffer) == ' ')
		;
	if (*buffer == '\\' || *buffer == '|')
		return (*buffer);
	return (-1);
}

char			ft_src_closure(char *str)
{
	ssize_t		ret;
	char		*tmp;
	char		*buffer;

	buffer = str;
	while (*buffer)
	{
		ret = ft_istrchr(OPEN, *buffer);
		if (ret != -1)
		{
			buffer = ft_strchr(buffer, CLOSE[ret]);
			if (buffer == NULL)
				return ((char)CLOSE[ret]);
		}
		else if (ft_strchr(QUOTES, *buffer))
		{
			tmp = ft_strchr(buffer + 1, *buffer);
			if (tmp == NULL)
				return (*buffer);
			buffer = tmp;
		}
		buffer++;
	}
	return (ft_pipeslash(str, buffer));
}

int				ft_enter(t_command *command)
{
	char		ret;
	t_line		*line;

	ret = ft_src_closure(command->buffer);
	if (ret == -1)
	{
		ft_heredoc(command->buffer);
		return (EXIT_SUCCESS);
	}
	ft_end(command);
	if (ret == '|')
	{
		ft_add_char(' ', command);
		tputs(tgetstr("do", NULL), 1, ft_rputchar);
	}
	else
		ft_add_char('\n', command);
	if (!(line = ft_memalloc(sizeof(t_line))))
		ft_malloc_error();
	line->first_x = 3;
	ft_lineadd(&(command->line), line);
	ft_putchar(ret);
	ft_putstr("> ");
	command->select = line;
	return (EXIT_FAILURE);
}

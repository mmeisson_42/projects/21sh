/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sher_access.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 01:29:36 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 01:53:08 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int		ft_sher_access(char *path)
{
	struct stat		s_stat;

	if (access(path, F_OK) == 0)
	{
		if (stat(path, &s_stat) == -1 || !S_ISDIR(s_stat.st_mode))
		{
			if (access(path, X_OK) == 0)
				g_errno = OK;
			else
				g_errno = EACCES;
		}
		else
			g_errno = EISDIR;
	}
	else
		g_errno = ECOM;
	return (g_errno);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_redirections.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/23 13:31:05 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/18 16:43:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

extern char		*g_heredoc;

static void		ft_check_for_errors(char *file)
{
	struct stat		s_stat;

	if (access(file, F_OK) == -1)
		g_errno = ENOENT;
	else if (access(file, W_OK))
		g_errno = EACCES;
	else if (lstat(file, &s_stat) != -1 && S_ISDIR(s_stat.st_mode))
		g_errno = EISDIR;
	else
		g_errno = 100;
	if (g_errno != 0)
		ft_perror(file);
}

static void		ft_redir_in(t_redir *r)
{
	int		fd[2];

	if (ft_strlen(r->type) == 2)
	{
		pipe(fd);
		if (g_heredoc)
		{
			dup2(fd[0], 0);
			ft_putstr_fd(g_heredoc, fd[1]);
			close(fd[1]);
		}
		ft_strdel(&g_heredoc);
	}
	else
	{
		fd[0] = open(r->sec_part, O_RDONLY);
		if (fd[0] != -1)
			dup2(fd[0], 0);
		else
			ft_check_for_errors(r->sec_part);
	}
}

static int		ft_out_fd(int fd[2], t_redir *r)
{
	if ((r->sec_part)[1] == '-')
	{
		close(fd[0]);
		return (1);
	}
	fd[1] = ft_atoi(r->sec_part + 1);
	return (0);
}

static void		ft_redir_out(t_redir *r)
{
	int		fd[2];

	if (r->first_part)
		fd[0] = (r->first_part) ? ft_atoi(r->first_part) : 1;
	else
		fd[0] = 1;
	if (ft_parse_file_desc(r->sec_part, REDIRECTION) == 0)
	{
		if (ft_out_fd(fd, r) == 1)
			return ;
	}
	else
	{
		fd[1] = (ft_strlen(r->type) == 2) ?
			open(r->sec_part, O_RDWR | O_CREAT | O_APPEND, 0755) :
			open(r->sec_part, O_RDWR | O_CREAT | O_TRUNC, 0755);
		if (fd[1] == -1)
		{
			ft_check_for_errors(r->sec_part);
			return ;
		}
	}
	dup2(fd[1], fd[0]);
	if (ft_parse_file_desc(r->sec_part, REDIRECTION) != 0)
		close(fd[1]);
}

void			ft_manage_descriptors(t_redir *r)
{
	while (r)
	{
		g_errno = OK;
		if (*(r->type) == '>')
			ft_redir_out(r);
		else
			ft_redir_in(r);
		if (g_errno != OK)
			return ;
		r = r->next;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cd.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 16:04:28 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/18 17:12:38 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

static int		ft_get_fromenv(char *env, char **curr_path, char **environ)
{
	if ((*curr_path = ft_getenv(env, environ)))
	{
		if (!(*curr_path = ft_strdup(*curr_path)))
			ft_malloc_error();
		return (1);
	}
	return (0);
}

int				ft_get_path(char *path, char **curr_path, int sym,
		char **environ)
{
	char	*tmp;
	char	actual[1024];

	if (!path)
		return (ft_get_fromenv("HOME", curr_path, environ));
	else if (ft_strequ("-", path))
		return (ft_get_fromenv("OLDPWD", curr_path, environ));
	else
	{
		bzero(actual, 1024);
		if (*path != '/')
		{
			if ((tmp = ft_getenv("PWD", environ)))
				ft_strncpy(actual, tmp, 1023);
			else
				getcwd(actual, 1023);
		}
		*curr_path = ft_pathjoin(actual, path);
		if (sym)
			ft_resolve_dot(*curr_path);
	}
	return (1);
}

int				ft_get_sym(char **av, int *sym)
{
	size_t		i;

	i = 1;
	*sym = 1;
	while (av[i] && av[i][0] == '-' && av[i][1])
	{
		if (ft_strequ(av[i], "-P"))
		{
			*sym = 0;
		}
		else if (ft_strequ(av[i], "-L"))
			*sym = 1;
		else
		{
			*sym = -1;
			return (-1);
		}
		i++;
	}
	return (i);
}

void			ft_manage_pwd(int sym, char *path, t_env *e)
{
	char		*oldpwd;

	if ((oldpwd = ft_getenv("PWD", e->env)))
		ft_envadd("OLDPWD", oldpwd, e);
	if (sym)
		ft_envadd("PWD", path, e);
	else
		ft_envadd("PWD", getcwd(NULL, 0), e);
}

int				ft_cd(int ac, char **av, t_env *e)
{
	int		sym;
	int		ind;
	char	*cur_path;

	ind = ft_get_sym(av, &sym);
	if (ind == -1 || ac > 12)
		return (ft_opt_error("cd [-P | -L] [path]", -1));
	if (av[ind] && av[ind + 1])
		return (ft_arg_error(0));
	if (!(ft_get_path(av[ind], &cur_path, sym, e->env)))
	{
		g_errno = ENOENT;
		return (EXIT_FAILURE);
	}
	if (chdir(cur_path) != -1)
	{
		ft_manage_pwd(sym, cur_path, e);
		free(cur_path);
		return (0);
	}
	else
	{
		g_errno = ENOENT;
		return (ft_perror(""));
	}
}

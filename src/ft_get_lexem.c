/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_lexem.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 14:52:05 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/04 04:04:55 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

char		*ft_is_pipe(char **command)
{
	char		*ret;

	if (ft_strnequ(*command, L_PIPE, 1))
	{
		if (!(ret = ft_strdup(L_PIPE)))
			ft_malloc_error();
		(*command)++;
		return (ret);
	}
	return (NULL);
}

char		*ft_is_red(char **command)
{
	char		*ret;

	if (ft_strchr(L_RED, (*command)[0]))
	{
		if ((*command)[0] == (*command)[1])
		{
			if (!(ret = ft_strndup(*command, 2)))
				ft_malloc_error();
			*command += 2;
		}
		else
		{
			if (!(ret = ft_strndup(*command, 1)))
				ft_malloc_error();
			(*command)++;
		}
		return (ret);
	}
	return (NULL);
}

char		*ft_is_stop(char **command)
{
	const char	*stop[] = {
		"&&", "||", ";", NULL,
	};
	size_t		i;
	char		*ret;
	size_t		len;

	i = 0;
	while (stop[i])
	{
		len = ft_strlen(stop[i]);
		if (ft_strnequ(*command, stop[i], len))
		{
			if (!(ret = ft_strndup(*command, len)))
				ft_malloc_error();
			*command += len;
			return (ret);
		}
		i++;
	}
	return (NULL);
}

static char	*ft_text_get(char *str, char **command)
{
	char		*ret;

	if ((str = ft_strchr(*command + 1, **command)))
	{
		if (!(ret = ft_strndup(*command, str - *command + 1)))
			ft_malloc_error();
		*command = str + 1;
	}
	else
	{
		if (!(ret = ft_strdup(*command)))
			ft_malloc_error();
		*command += ft_strlen(*command);
	}
	return (ret);
}

char		*ft_is_text(char **command)
{
	char		*str;
	char		*ret;

	str = NULL;
	if (ft_strchr(L_SEP, **command))
		ret = ft_text_get(str, command);
	else
	{
		str = *command;
		while (*str && *str != ' ' && !ft_strchr(NORME_PACKAGE, *str) &&
				ft_strncmp(str, "&&", 2) && ft_strncmp(str, "||", 2) &&
				*str != ';')
			str++;
		if (!(ret = ft_strndup(*command, str - *command)))
			ft_malloc_error();
		*command = str;
	}
	return (ret);
}

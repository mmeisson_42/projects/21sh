/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_export.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/24 03:59:13 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 04:17:37 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

extern t_hash		*g_table[TABLE_SIZE];

static int		ft_get_option(char ***av, int *write)
{
	char		**argv;
	int			ret;

	argv = *av;
	ret = 0;
	*write = 0;
	while (*argv && *argv[0] == '-')
	{
		if (!ft_strequ(*argv, "-p"))
		{
			ret = ft_opt_error("env [-p] [var(=value)]", *argv[1]);
			break ;
		}
		*write = 1;
		argv++;
	}
	*av = argv;
	return (ret);
}

static void		ft_set_args(char *args[2], char **av)
{
	char		*tmp;

	if ((tmp = ft_strchr(*av, '=')))
	{
		if (!(args[0] = ft_strndup(*av, tmp - *av)) ||
				(!(args[1] = ft_strdup(tmp + 1))))
			ft_malloc_error();
	}
	else
	{
		if (!(args[0] = ft_strdup(*av)) ||
			(!(args[1] = ft_memalloc(1))))
			ft_malloc_error();
	}
}

int				ft_export(int ac, char **av, t_env *e)
{
	int			ret;
	int			write;
	char		*args[2];

	ft_bzero(args, sizeof(char*) * 2);
	av++;
	ret = ft_get_option(&av, &write);
	if (ret == 0 && ac > -1)
		while (*av)
		{
			ft_set_args(args, av);
			if (ft_envadd(args[0], args[1], e) == 0)
			{
				if (write)
					ft_putendl(*av);
				if (ft_strequ(args[0], "PATH"))
					ft_load_hash_paths(ft_getenv("PATH", e->env), g_table);
			}
			ft_strdel(&args[0]);
			ft_strdel(&args[1]);
			av++;
		}
	return (ret);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parser.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/18 06:14:39 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 14:50:00 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

t_type			ft_get_type(t_lex *node, t_type last)
{
	const t_type	rules_set[7][8] = {
		{ OPTION, PIPE, REDIRECTION, FILE_DESC, STOP, -1 },
		{ EXEC, -1 },
		{ OPTION, PIPE, REDIRECTION, STOP, -1 },
		{ FILE_DESC, FILE_NAME, -1 },
		{ PIPE, REDIRECTION, STOP, OPTION, -1 },
		{ PIPE, REDIRECTION, STOP, OPTION, -1 },
		{ EXEC, STOP, -1 },
	};
	static int		(*fct[])() = {
		ft_parse_exec,
		ft_parse_pipe,
		ft_parse_option,
		ft_parse_redirection,
		ft_parse_file_desc,
		ft_parse_file_name,
		ft_parse_stop,
	};
	int				type;

	type = -1;
	while (rules_set[last][++type] != -1)
		if (fct[rules_set[last][type]](node->str, last) == 0)
			return (rules_set[last][type]);
	return (-1);
}

void			ft_pipestop(t_strstr **linkstr, t_strstr **c)
{
	if (*c)
	{
		if (!((*c)->next = ft_memalloc(sizeof(**linkstr))))
			ft_malloc_error();
		(*c) = (*c)->next;
	}
	else
	{
		if (!(*linkstr = ft_memalloc(sizeof(**linkstr))))
			ft_malloc_error();
		*c = *linkstr;
	}
}

t_lex			*ft_preprepare_parse(t_lex *l, t_strstr *curr,
		t_type last, t_lex **save_lex)
{
	if (last == REDIRECTION && (*save_lex)->type == OPTION &&
			(l->str)[0] == '>' && ft_parse_file_desc((*save_lex)->str, -1) == 0)
	{
		(*save_lex)->type = FILE_DESC;
		ft_strdellast(&(curr->str));
	}
	l->type = last;
	*save_lex = l;
	return (l->next);
}

t_strstr		*ft_prepare_parse(t_lex *l)
{
	t_type		last;
	t_strstr	*linkstr;
	t_strstr	*curr;
	t_lex		*save_lex;

	last = STOP;
	linkstr = NULL;
	curr = NULL;
	while (l)
	{
		if (last == PIPE || last == STOP)
			ft_pipestop(&linkstr, &curr);
		last = ft_get_type(l, last);
		if (last == OPTION)
			ft_stradd(&(curr->str), l->str);
		if (last == T_NULL)
		{
			ft_putendl_fd("PARSE_ERROR", 2);
			ft_strstrdel(linkstr);
			return (NULL);
		}
		else
			l = ft_preprepare_parse(l, curr, last, &save_lex);
	}
	return (linkstr);
}

t_start_node	*ft_parser(t_lex *l)
{
	t_start_node	*parsed_tree;
	t_strstr		*args;

	if (!(args = ft_prepare_parse(l)))
		return (NULL);
	parsed_tree = ft_get_parse(l, args);
	ft_strstrdel(args);
	return (parsed_tree);
}

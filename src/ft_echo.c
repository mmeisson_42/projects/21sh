/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_echo.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 22:14:25 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/04 00:32:25 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

char		ft_get_char(char c)
{
	const t_spechar		spec[8] = {
		{ 'a', '\a' },
		{ 'b', '\b' },
		{ 'f', '\f' },
		{ 'n', '\n' },
		{ 'r', '\r' },
		{ 't', '\t' },
		{ 'v', '\v' },
		{ '\\', '\\' },
	};
	int					i;

	i = 0;
	while (i < 8)
	{
		if (spec[i].spec == c)
			return (spec[i].val);
		i++;
	}
	return (0);
}

char		*ft_get_echo(char *av)
{
	char				*ret;
	size_t				i;

	i = 0;
	if (!(ret = ft_strnew(ft_strlen(av))))
		ft_malloc_error();
	while (*av)
	{
		if (*av == '\\')
		{
			if ((ret[i] = ft_get_char(av[1])))
			{
				i++;
				av += 2;
			}
			else
				av++;
		}
		else
			ret[i++] = *(av++);
	}
	return (ret);
}

int			ft_echoption(char **av, int *i)
{
	int		nl;

	nl = (*av && ft_strequ(av[1], "-n")) ? 0 : 1;
	*i = 1;
	while (av[*i] && ft_strequ(av[*i], "-n"))
		(*i)++;
	return (nl);
}

int			ft_echo(int ac, char **av, t_env *env)
{
	int		i;
	int		nl;
	char	*str;

	if (ac > 0 && env)
	{
		nl = ft_echoption(av, &i);
		while (av[i])
		{
			str = ft_get_echo(av[i]);
			ft_putstr(str);
			free(str);
			i++;
			if (av[i])
				ft_putchar(' ');
		}
		if (ft_strlen(av[--i]) > 2 && ft_strequ("\\c", av[i] +
					ft_strlen(av[i]) - 2))
			nl = 0;
		if (nl)
			ft_putchar('\n');
	}
	return (EXIT_SUCCESS);
}

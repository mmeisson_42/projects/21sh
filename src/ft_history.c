/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_history.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 16:02:46 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/04 09:31:08 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void		ft_arrdown(t_command *command, t_command **history)
{
	t_command		*h;

	h = *history;
	if (h && h->prev)
	{
		h = h->prev;
		ft_home(command);
		tputs(tgetstr("sc", NULL), 1, ft_rputchar);
		tputs(tgetstr("cd", NULL), 1, ft_rputchar);
		ft_putstr(h->buffer);
		tputs(tgetstr("rc", NULL), 1, ft_rputchar);
		command->pos -= command->select->pos;
		command->select->pos = 0;
		command->len = command->pos + h->len;
		command->select->len = h->len;
		if (command->buff_size < command->len)
			command->buffer = ft_strrealloc(command->buffer,
					command->len + BUFFER_SIZE);
		ft_strcpy(command->buffer + command->pos, h->buffer);
		ft_end(command);
		*history = h;
	}
}

void		ft_arrup(t_command *command, t_command **history)
{
	t_command		*h;

	h = *history;
	if (h && h->next)
	{
		h = h->next;
		ft_home(command);
		tputs(tgetstr("sc", NULL), 1, ft_rputchar);
		tputs(tgetstr("cd", NULL), 1, ft_rputchar);
		ft_putstr(h->buffer);
		tputs(tgetstr("rc", NULL), 1, ft_rputchar);
		command->pos -= command->select->pos;
		command->select->pos = 0;
		command->len = command->pos + h->len;
		command->select->len = h->len;
		if (command->buff_size < command->len)
			command->buffer = ft_strrealloc(command->buffer,
					command->len + BUFFER_SIZE);
		ft_strcpy(command->buffer + command->pos, h->buffer);
		ft_end(command);
		*history = h;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hash.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/22 08:10:11 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 19:43:23 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void			ft_insert_hash_key(char *key, char *value,
		t_hash *table[TABLE_SIZE])
{
	size_t		index;
	t_hash		*node;

	index = get_hash(key);
	if (!(node = malloc(sizeof(t_hash))))
		ft_malloc_error();
	node->key = ft_strdup(key);
	node->value = ft_strdup(value);
	node->next = table[index];
	table[index] = node;
}

void			ft_del_hash(t_hash **hash)
{
	if (*hash)
	{
		ft_del_hash(&((*hash)->next));
		free((*hash)->key);
		free((*hash)->value);
		free(*hash);
		*hash = NULL;
	}
}

void			ft_clear_table(t_hash *table[TABLE_SIZE])
{
	size_t		i;

	i = 0;
	while (i < TABLE_SIZE)
	{
		ft_del_hash(&table[i]);
		i++;
	}
}

static void		ft_inserthash(char **paths, struct dirent *file,
		t_hash *table[TABLE_SIZE])
{
	char			*value;

	value = NULL;
	if ((*paths)[ft_strlen(*paths) - 1] == '/')
		value = ft_strjoin(*paths, file->d_name);
	else
	{
		value = ft_strjoin(*paths, "/");
		value = ft_strover(value, file->d_name);
	}
	if (value && access(value, X_OK) == 0)
		ft_insert_hash_key(file->d_name, value, table);
	ft_strdel(&value);
}

t_hash			**ft_load_hash_paths(const char *path,
		t_hash *table[TABLE_SIZE])
{
	const char		*defaultpath = "/usr/bin/:/bin/";
	char			**paths;
	DIR				*dir;
	struct dirent	*file;
	ssize_t			i;

	i = -1;
	if (!path || !*path)
		path = defaultpath;
	if (!(paths = ft_strsplit(path, ':')))
		ft_malloc_error();
	ft_clear_table(table);
	while (paths[++i])
	{
		dir = opendir(paths[i]);
		if (dir)
		{
			while ((file = readdir(dir)))
				ft_inserthash(paths + i, file, table);
			closedir(dir);
		}
		ft_strdel(paths + i);
	}
	free(paths);
	return (table);
}

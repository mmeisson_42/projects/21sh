/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_hist.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/06 22:05:25 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 22:07:39 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

t_command			*ft_get_hist(t_command *command, t_command *history)
{
	t_command	*hist_copy;

	if (!(hist_copy = ft_memalloc(sizeof(t_command))))
		ft_malloc_error();
	hist_copy->buffer = command->buffer;
	hist_copy->next = ft_historicdup(history, command);
	command->select->first_x = ft_print_prompt();
	return (hist_copy);
}

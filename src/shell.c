/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 06:20:45 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 21:19:20 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

t_global		g_datas;
t_hash			*g_table[TABLE_SIZE];
t_env			*g_env = NULL;

int			main(void)
{
	extern char			**environ;
	t_command			*history;
	t_command			*command;
	t_start_node		*parsed;
	char				*b;

	history = NULL;
	g_env = ft_init_datas(environ);
	ft_signal_managing();
	while (42)
	{
		command = ft_get_command(history);
		ft_commandadd_front(&history, command);
		b = command->buffer;
		while ((parsed = ft_lexerparser(&b)))
		{
			b = NULL;
			g_errno = OK;
			ft_sherialize(parsed, NULL);
			wait(NULL);
			ft_parsedel(parsed);
		}
	}
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parsedel.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/06 14:15:53 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 14:41:18 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

static void		ft_delred(t_redir *r)
{
	if (r)
	{
		ft_delred(r->next);
		ft_strdel(&(r->first_part));
		ft_strdel(&(r->sec_part));
		ft_strdel(&(r->type));
	}
}

void			ft_parsedel(t_start_node *parse)
{
	char		**tab;
	size_t		i;

	if (parse)
	{
		i = 1;
		ft_parsedel(parse->next);
		ft_parsedel(parse->pipe);
		ft_strdel(&(parse->exec));
		ft_delred(parse->redir);
		tab = parse->argv;
		if (tab)
		{
			while (tab[i])
				free(tab[i++]);
			free(tab);
		}
		free(parse);
	}
}

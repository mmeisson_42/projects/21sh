/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sher_built.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/05 16:01:41 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 21:31:52 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

extern t_env	*g_env;

static int	(*ft_get_builtin(char *exec))()
{
	const t_int_fct		builtins[8] = {
		{ "cd", ft_cd },
		{ "env", ft_env },
		{ "export", ft_export },
		{ "unset", ft_unset },
		{ "setenv", ft_setenv },
		{ "unsetenv", ft_unsetenv },
		{ "echo", ft_echo },
		{ "exit", ft_exit },
	};
	int					i;

	i = -1;
	while (++i < 8)
	{
		if (ft_strequ(exec, builtins[i].str))
			return (builtins[i].fct);
	}
	return (NULL);
}

static void	ft_desc_saviour(void)
{
	static int		desc[2] = { -1, -1 };

	if (desc[0] == -1)
	{
		desc[0] = dup(0);
		desc[1] = dup(1);
	}
	else
	{
		dup2(desc[0], 0);
		dup2(desc[1], 1);
		desc[0] = -1;
	}
}

/*
** Cause actually there is no implemented builtin that read stdin,
** I don't give a shit
** To dup desc[0] on this stdin
*/

int			ft_builtin(t_start_node *node, int desc[2], char **environ)
{
	int		(*fct)();
	int		ret;

	if (environ)
		;
	if (!(fct = ft_get_builtin(node->exec)))
		return (-1);
	ft_desc_saviour();
	if (desc[0] != -1)
	{
		close(desc[1]);
		close(desc[0]);
	}
	if (node->pipe)
	{
		close(desc[0]);
		dup2(desc[1], 1);
	}
	else
		desc[0] = -1;
	ft_manage_descriptors(node->redir);
	ret = fct(node->argc, node->argv, g_env);
	ft_desc_saviour();
	return (ret);
}

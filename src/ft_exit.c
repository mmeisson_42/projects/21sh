/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 14:01:09 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 04:55:35 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

extern t_global			g_datas;

int		ft_exit(int ac, char **av, t_env *e)
{
	int		ret;

	if (e)
		;
	if (ac == 1 || ac == 2)
	{
		ret = (ac == 1) ? 0 : ft_atoi(av[1]);
		ft_shutdown(ret);
	}
	else
		ret = ft_opt_error("exit (code)", -1);
	return (ret);
}

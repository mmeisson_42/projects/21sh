/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lexerparser.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 18:00:12 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 20:14:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

static void			ft_dellex(t_lex *l)
{
	if (l)
	{
		ft_dellex(l->next);
		ft_strdel(&(l->str));
		free(l);
	}
}

static t_lex		*ft_lexinit(char **str)
{
	t_lex	*buffer;

	if (*str)
	{
		buffer = ft_lexer(*str);
		return (buffer);
	}
	else
		return (NULL);
}

t_start_node		*ft_lexerparser(char **str)
{
	static t_lex	*buffer = NULL;
	t_lex			*lexem;
	t_lex			*tmp;
	t_start_node	*parsed;

	if (!buffer && !(buffer = ft_lexinit(str)))
		return (NULL);
	parsed = NULL;
	if (buffer)
	{
		tmp = buffer;
		lexem = buffer;
		while (tmp && ft_strcmp(tmp->str, ";"))
			tmp = tmp->next;
		if (tmp)
		{
			buffer = tmp->next;
			tmp->next = NULL;
		}
		else
			buffer = NULL;
		parsed = ft_parser(lexem);
		ft_dellex(lexem);
	}
	return (parsed);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 01:51:22 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/04 02:06:30 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

static void		ft_unselect(t_command *c)
{
	if (c->selection)
	{
		while (c->selection != c->buffer + c->pos)
			ft_arrleft(c);
		tputs(tgetstr("me", NULL), 1, ft_rputchar);
		tputs(tgetstr("sc", NULL), 1, ft_rputchar);
		tputs(tgetstr("cd", NULL), 1, ft_rputchar);
		ft_putstr(c->buffer + c->pos);
		tputs(tgetstr("rc", NULL), 1, ft_rputchar);
	}
}

static int		ft_opts(t_command *c, t_command *command, char buffer[12])
{
	if (ft_strequ(buffer, O_C))
	{
		ft_optc(command);
		ft_unselect(c);
		return (1);
	}
	else if (ft_strequ(buffer, O_X))
	{
		ft_optx(command);
		ft_unselect(c);
		return (1);
	}
	return (0);
}

static void		ft_mselect(t_command *c)
{
	tputs(tgetstr("ic", NULL), 1, ft_rputchar);
	tputs(tgetstr("mr", NULL), 1, ft_rputchar);
	ft_putchar(c->buffer[c->pos]);
	c->select_size++;
	tputs(tgetstr("me", NULL), 1, ft_rputchar);
	c->pos++;
	c->select->pos++;
}

static int		ft_selarrow(t_command *c, t_command *command, char buffer[12])
{
	if (ft_strequ(buffer, ARR_RIGHT))
	{
		if (c->select->pos < c->select->len)
			ft_mselect(c);
		return (1);
	}
	else if (ft_strequ(buffer, ARR_LEFT))
	{
		if (c->select_size < 2)
		{
			ft_unselect(c);
			return (2);
		}
		c->select_size--;
		ft_putchar(c->buffer[c->pos]);
		tputs(tgetstr("le", NULL), 1, ft_rputchar);
		ft_arrleft(command);
		return (1);
	}
	return (0);
}

void			ft_select(t_command *command)
{
	t_command	*c;
	char		buffer[12];
	int			ret;

	c = command;
	c->selection = c->buffer + c->pos;
	c->select_size = 0;
	while (1)
	{
		ft_bzero(buffer, 12);
		read(1, buffer, 11);
		ret = ft_selarrow(c, command, buffer);
		if (ret == 2)
			return ;
		else if (ret == 0 && ft_opts(c, command, buffer) == 1)
			return ;
		else if (ret == 0)
		{
			ft_unselect(c);
			return ;
		}
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pars_identifier2.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/29 14:11:04 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 04:27:34 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int			ft_parse_file_name(char *str, t_type last)
{
	static int		(*fct[])() = {
		ft_parse_pipe,
		ft_parse_redirection,
		ft_parse_stop,
		NULL,
	};
	int				i;

	i = 0;
	while (fct[i])
	{
		if (fct[i](str, last) == 0)
			return (1);
		i++;
	}
	return (0);
}

int			ft_parse_stop(char *str, t_type last)
{
	if (last)
		;
	if (ft_strnequ(str, "&&", 2) || ft_strnequ(str, "||", 2)
			|| ft_strequ(str, ";"))
		return (0);
	return (1);
}

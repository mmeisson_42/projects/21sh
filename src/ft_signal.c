/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/28 00:12:46 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 02:26:52 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

extern t_global		g_datas;

static void		ft_signal(int sig)
{
	if (sig != 0 && g_datas.proc_status == 0)
	{
		ft_putchar('\n');
		ft_shutdown(g_errno);
	}
}

void			ft_signal_managing(void)
{
	size_t		i;

	i = 1;
	while (i < 30)
	{
		if (i != 28 && i != 20)
			signal(i, ft_signal);
		i++;
	}
}

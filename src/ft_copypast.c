/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_copypast.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 15:57:15 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/04 02:07:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

char		*g_buffer = NULL;

void		ft_optc(t_command *command)
{
	if (g_buffer != NULL)
		ft_strdel(&g_buffer);
	if (command->selection)
	{
		if (!(g_buffer = ft_strndup(command->selection, command->select_size)))
			ft_malloc_error();
	}
}

void		ft_optv(t_command *command)
{
	size_t		len;
	t_command	*c;

	if (g_buffer && *g_buffer)
	{
		c = command;
		len = ft_strlen(g_buffer);
		while (c->buff_size <= c->len + len)
		{
			c->buff_size += BUFFER_SIZE;
			c->buffer = ft_strrealloc(c->buffer, c->buff_size);
		}
		ft_memmove(c->buffer + c->pos + len, c->buffer + c->pos,
				c->len - c->pos);
		ft_memmove(c->buffer + c->pos, g_buffer, c->select_size);
		c->len += len;
		c->select->len += len;
		tputs(tgetstr("cd", NULL), 1, ft_rputchar);
		tputs(tgetstr("sc", NULL), 1, ft_rputchar);
		ft_putstr(c->buffer + c->pos);
		tputs(tgetstr("rc", NULL), 1, ft_rputchar);
	}
}

void		ft_optx(t_command *command)
{
	t_command	*c;
	size_t		len;

	c = command;
	len = c->select_size;
	if (g_buffer != NULL)
		ft_strdel(&g_buffer);
	if (c->selection)
	{
		g_buffer = ft_strndup(c->selection, len);
		ft_strcpy(c->selection, c->selection + c->select_size);
		c->len -= c->select_size;
		c->select->len -= c->select_size;
	}
}

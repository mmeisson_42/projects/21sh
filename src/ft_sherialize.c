/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sherialize.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/05 15:02:45 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/18 16:49:31 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

extern t_env		*g_env;
extern t_hash		*g_table[TABLE_SIZE];

char				*ft_get_from_path(char *exec, char **environ)
{
	char		*path;
	char		**paths;
	char		*cmd;
	size_t		i;

	g_errno = OK;
	if (!(path = ft_getenv("PATH", environ)) || !*path)
		path = "/usr/bin/:/bin/";
	if (!(paths = ft_strsplit(path, ':')))
		ft_malloc_error();
	i = -1;
	while (paths[++i])
	{
		cmd = ft_pathjoin(paths[i], exec);
		if (ft_sher_access(cmd) == OK)
		{
			ft_stringtab_del(&paths);
			return (cmd);
		}
	}
	ft_stringtab_del(&paths);
	return (NULL);
}

static char			*ft_get_exec(char *exec, char **environ)
{
	char		*command;

	command = NULL;
	if (exec)
	{
		if (ft_strnequ(exec, "./", 2) || *exec == '/')
			return (exec);
		else if (environ)
			command = ft_get_from_path(exec, environ);
		else
		{
			command = ft_get_hash_value(exec, g_table);
			if (command == NULL)
				g_errno = ECOM;
		}
	}
	return (command);
}

static void			ft_manage_xcution(t_start_node *node, char **environ)
{
	char		*command;

	if ((command = ft_get_exec(node->exec, environ)))
	{
		if (environ == NULL)
			environ = g_env->env;
		execve(command, node->argv, environ);
	}
	if (g_errno != 0)
		ft_perror("");
	exit(127);
}

static pid_t		ft_under(t_start_node *node, int desc[2], char **environ)
{
	int		d[2];
	pid_t	f;

	if (node->pipe)
		pipe(d);
	else
		d[0] = -1;
	f = fork();
	if (f == 0)
	{
		if (desc[0] != -1)
			dup2(desc[0], 0);
		if (node->pipe)
		{
			close(d[0]);
			dup2(d[1], 1);
		}
		ft_manage_descriptors(node->redir);
		ft_manage_xcution(node, environ);
	}
	close(d[1]);
	desc[0] = d[0];
	desc[1] = d[1];
	return (f);
}

int					ft_sherialize(t_start_node *node, char **environ)
{
	int		ret;
	int		desc[2];

	ret = 0;
	desc[0] = -1;
	if (node)
	{
		ret = ft_sherialize(node->next, environ);
		if (node->cond == 0 || (node->cond == -1 && ret != 0) ||
				(node->cond == 1 && ret == 0))
		{
			while (node)
			{
				ret = ft_builtin(node, desc, environ);
				if (ret == -1)
					ret = ft_under(node, desc, environ);
				node = node->pipe;
			}
			waitpid(ret, &ret, 0);
			ret = WEXITSTATUS(ret);
		}
	}
	return (ret);
}

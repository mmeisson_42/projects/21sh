/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 14:49:12 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 01:01:52 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

extern t_hash		*g_table[TABLE_SIZE];

int		ft_setenv(int ac, char **av, t_env *e)
{
	int				overwrite;

	if (ac > 2 && ac < 5)
	{
		overwrite = (ac == 4 && ft_strequ(av[3], "0")) ? 0 : 1;
		if (*(av[1]) && (overwrite || !ft_getenv(av[1], e->env)))
		{
			if (ft_envadd(av[1], av[2], e) == EXIT_SUCCESS &&
					ft_strequ(av[1], "PATH"))
			{
				ft_load_hash_paths(ft_getenv(av[1], e->env), g_table);
			}
		}
		return (EXIT_SUCCESS);
	}
	ft_opt_error("[var] [content] (overwrite)", -1);
	return (EXIT_FAILURE);
}

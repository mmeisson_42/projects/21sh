/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_homend.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 15:54:54 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/01 04:58:45 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void		ft_home(t_command *command)
{
	t_line		*l;

	l = command->select;
	while (l->pos != 0)
		ft_arrleft(command);
}

void		ft_end(t_command *command)
{
	t_line		*l;

	l = command->select;
	while (l->pos != l->len)
		ft_arrright(command);
}

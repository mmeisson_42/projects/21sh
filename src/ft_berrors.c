/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_berrors.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 00:21:11 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 01:13:13 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int		ft_opt_error(const char *usage, char opt)
{
	ft_putstr_fd("Option's error -- ", 2);
	if (opt > 0)
		ft_putchar_fd(opt, 2);
	ft_putchar_fd('\n', 2);
	ft_putstr_fd("usage: ", 2);
	ft_putendl_fd(usage, 2);
	return (EXIT_FAILURE);
}

int		ft_arg_error(int type)
{
	const char		*errors[] = {
		"Too many args",
		"Too much args",
		"Bad args number",
		"Need an other arg",
	};

	ft_putstr_fd("21sh: ", 2);
	if (type < 4)
		ft_putendl_fd(errors[type], 2);
	else
		ft_putendl_fd("Unknown error", 2);
	return (EXIT_FAILURE);
}

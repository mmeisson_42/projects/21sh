/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 06:22:28 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/06 22:17:24 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHELL_H
# define SHELL_H

# include "libft.h"
# include <termios.h>
# include <termcap.h>
# include <unistd.h>
# include <stdio.h>
# include <dirent.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/ioctl.h>
# include <sys/types.h>
# include <sys/wait.h>

# define PROMPT_SIZE 31
# define PROMPT_IND_SIZE 11
# define ENV_MAX_SIZE 1024

# define DEFAULT "\e[0m"
# define L_GREEN "\e[92m"
# define L_ROUGE "\e[91m"

# define DEL "\177"
# define DELETE "\33\133\63\176"
# define ENTER "\012"
# define HOME "\033\133\110"
# define END "\033\133\106"
# define ARR_UP "\033\133\101"
# define ARR_DOWN "\033\133\102"
# define ARR_RIGHT "\033\133\103"
# define ARR_LEFT "\033\133\104"
# define OPT_UP "\033\033\133\101"
# define OPT_DOWN "\033\033\133\102"
# define OPT_RIGHT "\033\033\133\103"
# define OPT_LEFT "\033\033\133\104"
# define O_C "\303\247"
# define O_V "\342\210\232"
# define O_X "\342\211\210"
# define SELECT "\316\251"

# define L_PIPE "|"
# define L_RED "<>"
# define L_SEP "'\""
# define L_STOP ";&|"
# define QUOTES "\"'`"
# define OPEN "([{"
# define CLOSE ")]}"

# define NORME_PACKAGE L_PIPE L_RED L_SEP

# define TABLE_SIZE 2048
# define MULTIPLIER 37
# define BUFFER_SIZE 256

typedef struct s_env		t_env;
typedef struct s_global		t_global;
typedef struct s_lex		t_lex;
typedef struct s_parse		t_parse;
typedef struct s_fct		t_fct;
typedef struct s_int_fct	t_int_fct;
typedef struct s_str		t_str;
typedef struct s_strstr		t_strstr;
typedef struct s_redir		t_redir;
typedef struct s_start_node	t_start_node;
typedef struct s_hash		t_hash;
typedef struct s_spechar	t_spechar;
typedef struct s_command	t_command;
typedef struct s_line		t_line;

typedef enum e_tererr		t_tererr;
typedef enum e_type			t_type;
typedef enum e_errno		t_errno;

enum			e_tererr
{
	TERM_UNSET,
	TERM_NFOUND,
	TERMIOS_NFOUND,
};

enum			e_errno
{
	OK,
	EPERM,
	ENOENT,
	ESRCH,
	EINTR,
	EIO,
	ENXIO,
	E2BIG,
	ENOEXEC,
	EBADF,
	ECHILD,
	EDEADLK,
	ENOMEM,
	EACCES,
	EFAULT,
	ENOTBLK,
	EBUSY,
	EEXIST,
	EXDEV,
	ENODEV,
	ENOTDIR,
	EISDIR,
	EINVAL,
	ENFILE,
	EMFILE,
	ENOTTY,
	ETXTBSY,
	EFBIG,
	ENOSPC,
	ESPIPE,
	EROFS,
	EMLINK,
	EPIPE,
	EDOM,
	ERANGE,
	EAGAIN,
	EINPROGRESS,
	EALREADY,
	ECOM,
};

t_errno			g_errno;

struct			s_start_node
{
	t_start_node	*pipe;
	char			*exec;
	int				argc;
	int				cond;
	char			**argv;
	t_redir			*redir;
	t_start_node	*next;
};

enum			e_type
{
	EXEC,
	PIPE,
	OPTION,
	REDIRECTION,
	FILE_DESC,
	FILE_NAME,
	STOP,
	T_NULL = -1,
};

struct			s_hash
{
	t_hash		*next;
	char		*key;
	char		*value;
};

struct			s_redir
{
	char		*first_part;
	char		*sec_part;
	char		*type;
	t_redir		*next;
};

struct			s_fct
{
	char		*str;
	void		(*fct)();
};

struct			s_int_fct
{
	char		*str;
	int			(*fct)();
};

struct			s_str
{
	t_str		*next;
	char		*str;
	size_t		length;
};

struct			s_strstr
{
	t_strstr	*next;
	t_str		*str;
	size_t		length;
};

struct			s_spechar
{
	char		spec;
	char		val;
};

struct			s_lex
{
	t_lex		*next;
	char		*str;
	t_type		type;
};

struct			s_line
{
	t_line		*next;
	t_line		*prev;
	size_t		first_x;
	size_t		len;
	size_t		pos;
};

struct			s_command
{
	t_command	*prev;
	t_command	*next;
	char		*buffer;
	char		*selection;
	size_t		buff_size;
	size_t		len;
	size_t		pos;
	t_line		*select;
	t_line		*line;
	int			select_size;
};

struct			s_env
{
	char		*env[ENV_MAX_SIZE];
	int			env_size;
	int			sel_ind;
};

struct			s_global
{
	struct termios	origin;
	int				proc_status;
};

typedef int		(*t_built)();
t_built			ft_get_builtins(char *exec);

t_env			*ft_init_datas(char **environ);
void			ft_signal_managing(void);
t_command		*ft_get_command(t_command *command);
t_command		*ft_historicdup(t_command *history, t_command *command);
t_command		*ft_init_command(void);

size_t			ft_print_prompt();
char			*ft_pathjoin(char *p1, char *p2);
char			*ft_interpret(char *str);
int				ft_action(char *buffer, t_command *command,
		t_command **historic);

t_env			*ft_envinit(char **env);
char			*ft_getenv(char *src, char **env);
int				ft_envadd(char *var, char *content, t_env *e);
int				ft_envdel(char *var, t_env *e);
void			ft_commandadd_front(t_command **head, t_command *node);

void			ft_malloc_error(void);
void			ft_term_error(t_tererr e);
void			ft_shutdown(int error_code);

int				ft_rputchar(int c);
void			ft_stringtab_del(char ***str);
size_t			ft_get_command_index(t_command *command);

t_lex			*ft_lexer(char *str);
t_start_node	*ft_parser(t_lex *lexem);
t_start_node	*ft_lexerparser(char **str);

t_start_node	*ft_get_parse(t_lex *l, t_strstr *args);

void			ft_trimslash(char *str);
void			ft_resolve_dot(char *str);
char			*ft_get_from_path(char *exec, char **environ);
int				ft_builtin(t_start_node *start, int desc[2], char **environ);
char			*ft_get_variables(char *str);

char			*ft_is_pipe(char **str);
char			*ft_is_red(char **str);
char			*ft_is_stop(char **str);
char			*ft_is_text(char **str);

int				ft_parse_exec(char *str, t_type last);
int				ft_parse_option(char *str, t_type last);
int				ft_parse_pipe(char *str, t_type last);
int				ft_parse_file_desc(char *str, t_type last);
int				ft_parse_redirection(char *str, t_type last);
int				ft_parse_file_name(char *str, t_type last);
int				ft_parse_stop(char *str, t_type last);

t_lex			*ft_lexnew(char *str);
void			ft_lexadd(t_lex **head, t_lex *node);
void			ft_lineadd(t_line **line, t_line *node);
void			ft_stradd(t_str **lst, char *str);
void			ft_strstrdel(t_strstr *s);
void			ft_strdellast(t_str **s);
void			ft_redadd(t_redir **list, t_redir *node);

int				ft_perror(const char *msg);
int				ft_arg_error(int type);
int				ft_opt_error(const char *usage, char opt);
int				ft_sher_access(char *path);

void			ft_print_env(t_env *e);
void			ft_fill_env(t_env *benv, char **env);
int				ft_empty_env(char ***av);
void			ft_subgetter(t_lex **l, t_start_node **parse,
		t_strstr **args);
void			ft_get_red(t_lex **l, t_start_node *node);
void			ft_manage_exec(t_start_node *node, t_lex *l,
		t_strstr *args);

void			ft_parsefill(t_parse *parse, t_lex *lex, t_type type);
void			ft_parsedel(t_start_node *parse);

int				ft_sherialize(t_start_node *node, char **environ);
void			ft_manage_descriptors(t_redir *redirection);

size_t			ft_get_hash(const char *key);
size_t			get_hash(const char *key);
char			*ft_get_hash_value(const char *key,
		t_hash *table[TABLE_SIZE]);
void			ft_insert_hash_key(char *key, char *value,
		t_hash *table[TABLE_SIZE]);
t_hash			**ft_load_hash_paths(const char *path,
		t_hash *table[TABLE_SIZE]);

void			ft_add_char(char add, t_command *c);
void			ft_heredoc(char *str);
int				ft_enter(t_command *command);
t_command		*ft_get_hist(t_command *command, t_command *history);

void			ft_delete(t_command *command);
void			ft_deleteback(t_command *command);
void			ft_home(t_command *command);
void			ft_end(t_command *command);
void			ft_optup(t_command *command);
void			ft_optdown(t_command *command);
void			ft_optright(t_command *command);
void			ft_optleft(t_command *command);
void			ft_arrleft(t_command *command);
void			ft_arrright(t_command *command);
void			ft_select(t_command *command);
void			ft_optc(t_command *command);
void			ft_optv(t_command *command);
void			ft_optx(t_command *command);

void			ft_arrup(t_command *command, t_command **history);
void			ft_arrdown(t_command *command, t_command **history);

int				ft_cd(int ac, char **av, t_env *e);
int				ft_env(int ac, char **av, t_env *e);
int				ft_setenv(int ac, char **av, t_env *e);
int				ft_export(int ac, char **av, t_env *e);
int				ft_unsetenv(int ac, char **av, t_env *e);
int				ft_unset(int ac, char **av, t_env *e);
int				ft_exit(int ac, char **av, t_env *e);
int				ft_echo(int ac, char **av, t_env *e);

#endif
